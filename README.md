# Clone the repo

git clone https://shobitjain@bitbucket.org/shobitjain/movie.git

cd movie

## Install npm packages

Install the npm packages described in the package.json and verify that it works:

npm install

## Run Server

ng serve

Navigate to http://localhost:4200/