export class Movie {
	Title: string;
	Year: number;
	imdbID :string;
	Type: string;
	Poster: string;
  }
export class MovieDetails {
	Title: string;
	Year: number;
	Type: string;
	Poster: string;
	Plot: string;
	imdbID: string;
	Runtime: string;
	Genre: string;
  }