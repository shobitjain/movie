import { createAction,props } from '@ngrx/store';
export const search = createAction(
    '[List Component] Search', 
    props<{searchKey:string}>()
    );