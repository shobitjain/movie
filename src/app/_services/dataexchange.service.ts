import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Movie, MovieDetails } from '../movie';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class DataexchangeService {
	constructor(private httpclient: HttpClient) { }
	apiUrl = "http://www.omdbapi.com/?apikey=f79aeba3&";


	getMovie(movietitle: string): Observable<any> {
		return this.httpclient.get<Movie[]>(this.apiUrl +"s="+ movietitle);
	}
	getMovieDetail(iMDBRating: string): Observable<any> {
		return this.httpclient.get<MovieDetails[]>(this.apiUrl +"i="+iMDBRating);
	}

}
