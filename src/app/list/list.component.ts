import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DataexchangeService } from '../_services/dataexchange.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Movie } from '../movie';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { search } from '../movie.actions';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {
  searchModel = "";
  movieList: Movie[];
  constructor(private _DataexchangeService: DataexchangeService, private _activateRoute: ActivatedRoute,
    private _router: Router, private store: Store<{ searchData: string, listData: Array<Movie> }>) { }
  searchForm: FormGroup;

  ngOnInit() {
    this.searchForm = new FormGroup({ searchInput: new FormControl('') });
    this.store.select("searchData").subscribe((data) => {
      if (data)
        this.searchModel = data;
    });
    this.store.select("listData").subscribe((data) => {

      if (data)
        this.movieList = data;
    });
  }
  onSubmit() {
    if (this.searchForm.value["searchInput"]) {
      this.store.dispatch({
        type: 'searchMovie',
        payload: this.searchForm.value["searchInput"]
      });

      this._DataexchangeService.getMovie(this.searchForm.value["searchInput"]).subscribe(
        data => {

          if (data.Response === "False") {
            document.getElementById("alert").style.display = "block";
          }
          else {
            document.getElementById("alert").style.display = "none";
            this.store.dispatch({
              type: 'getMoviesList',
              payload: data.Search
            });
          }
        }
      );
    }
  }

}
