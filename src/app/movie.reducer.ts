export function movieSearchReducer(state: "", action) {
  switch (action.type) {
    case "searchMovie":
        return action.payload;
    default:
        return state;
    }
}

export function movieListReducer(state: [], action) {
    switch (action.type) {
      case "getMoviesList":
        return action.payload;
      default:
          return state;
      }
  }
// import { search } from './movie.actions';

// export interface State {
//     searchKey: string;
// }

// export const initialState: State = {
//     searchKey: ""
// };

// const _searchReducer = createReducer(
//   initialState,
//   on(search, state => ({searchKey: state.searchKey}))
// );

// export function searchReducer(state: State, action: Action) {
//     debugger
//     console.log(state);
//   return _searchReducer(state, action);
// }