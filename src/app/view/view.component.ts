import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataexchangeService } from '../_services/dataexchange.service';
import { MovieDetails } from '../movie';
import {Location} from '@angular/common';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  detailMovie: MovieDetails;
  idpass:string;
  constructor(private _location: Location,private _DataexchangeService: DataexchangeService,private _activateRoute: ActivatedRoute
    ) { }

  ngOnInit() {
    this.idpass = this._activateRoute.snapshot.params.id;
    console.log(this.idpass);
    return this._DataexchangeService.getMovieDetail(this.idpass).subscribe(
       data => {
         this.detailMovie = data;
       }
    );
  }
  backClicked() {
    this._location.back();
  }

}
