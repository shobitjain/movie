import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { DataexchangeService} from './_services/dataexchange.service';
import { AppRoutingModule } from './app-routing.module';
import { ViewComponent } from './view/view.component';
import { ListComponent } from './list/list.component';

import { StoreModule } from '@ngrx/store';
import { movieSearchReducer, movieListReducer } from './movie.reducer';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import '@angular/material/prebuilt-themes/indigo-pink.css';

@NgModule({
  declarations: [
    AppComponent,
    ViewComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot({ searchData: movieSearchReducer, listData: movieListReducer }),
    BrowserAnimationsModule
  ],
  providers: [DataexchangeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
